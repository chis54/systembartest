package com.ccwilcox.systembartest;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.widget.RelativeLayout;

import com.readystatesoftware.systembartint.SystemBarTintManager;
import com.readystatesoftware.systembartint.SystemBarTintManager.SystemBarConfig;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		int backgroundColor = Color.parseColor("#980000");		
		getActionBar().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		
		// Set up the nav and status bars 
		SystemBarTintManager tintManager = new SystemBarTintManager(this);
		tintManager.setStatusBarTintEnabled(true);
		tintManager.setStatusBarTintColor(backgroundColor);
		tintManager.setNavigationBarTintEnabled(true);
		tintManager.setNavigationBarTintColor(backgroundColor);
		
		SystemBarConfig config = tintManager.getConfig();
		RelativeLayout topRelativeLayout = (RelativeLayout) findViewById(R.id.topRelativeLayout);
		topRelativeLayout.setBackgroundColor(backgroundColor);
		topRelativeLayout.setPadding(0, config.getActionBarHeight() + config.getStatusBarHeight(), 0, config.getNavigationBarHeight());
	
	}
}
